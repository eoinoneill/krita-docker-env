FROM kdeorg/appimage-1804

MAINTAINER Dmitry Kazakov <dimula73@gmail.com>
MAINTAINER Eoin ONeill <eoinoneill1991@gmail.com>
RUN apt-get update && \
    apt-get -y install curl && \
    apt-get -y install emacs-nox && \
    apt-get -y install gitk git-gui && \
    apt-get -y install cmake-curses-gui gdb valgrind sysvinit-utils && \
    apt-get -y install nomacs && \
    apt-get -y install mesa-utils && \
    apt-get -y install libxcb-icccm4 libxcb-image0 libxcb-render-util0 libxcb-xinerama0

RUN update-alternatives --set gcc /usr/bin/gcc-7
RUN update-alternatives --set g++ /usr/bin/g++-7
    
ENV USRHOME=/home/appimage
WORKDIR ${USRHOME}

RUN chsh -s /bin/bash appimage

RUN locale-gen en_US.UTF-8

RUN echo 'export LC_ALL=en_US.UTF-8' >> ${USRHOME}/.bashrc && \
    echo 'export LANG=en_US.UTF-8'  >> ${USRHOME}/.bashrc && \
    echo "export PS1='\u@\h:\w>'"  >> ${USRHOME}/.bashrc && \
    echo 'source ~/devenv.inc' >> ${USRHOME}/.bashrc && \
    echo 'prepend PATH ~/bin/' >> ${USRHOME}/.bashrc

RUN mkdir -p ${USRHOME}/appimage-workspace/krita.appdir/usr && \
    mkdir -p ${USRHOME}/appimage-workspace/krita-build && \
    mkdir -p ${USRHOME}/appimage-workspace/deps/usr && \
    mkdir -p ${USRHOME}/bin && \
    mkdir -p ${USRHOME}/build && \
    mkdir -p /mnt/out && \
    mkdir -p /mnt/krita

COPY ./default-home/devenv.inc \
     ./default-home/.bash_aliases \
     ${USRHOME}/

COPY ./default-home/run_cmake.sh \
     ./default-home/build_krita_appimage.sh \
     ./default-home/bootstrap-deps.sh \
     ${USRHOME}/bin/

RUN chown appimage:appimage -R ${USRHOME}/
RUN chown appimage:appimage -R /mnt/*
RUN chmod a+rwx /tmp

USER appimage

CMD ${USRHOME}/bin/bootstrap-deps.sh && . ${USRHOME}/devenv.inc && /bin/bash


#ENTRYPOINT ["/bin/bash", "-c", "source ~/devenv.inc && env /bin/bash \"$@\""]

