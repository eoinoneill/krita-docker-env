#!/bin/bash

source_directory=${@:-/mnt/krita}

# If the user isn't specifying a source directory manually, let's
# default to a sane cmake build location...
if [[ ./ -ef ~ ]]; then
    mkdir -p ~/build
    cd ~/build
fi

cmake -DCMAKE_INSTALL_PREFIX=${KRITADIR} \
      -DCMAKE_BUILD_TYPE=Debug \
      -DKRITA_DEVS=ON \
      -DBUILD_TESTING=TRUE \
      -DHIDE_SAFE_ASSERTS=FALSE \
      -DPYQT_SIP_DIR_OVERRIDE=~/appimage-workspace/deps/usr/share/sip \
      ${source_directory}
