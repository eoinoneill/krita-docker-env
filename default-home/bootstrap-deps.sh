#!/bin/bash

if [ ! -d ./media ]; then
    mkdir ./media
fi

if [ ! -f ./media/krita-appimage-deps.tar ]; then
    (
        echo "Installing Krita Dependencies.."
        cd ~/media/
        wget https://binary-factory.kde.org/job/Krita_Nightly_Appimage_Dependency_Build/lastSuccessfulBuild/artifact/krita-appimage-deps.tar || exit 1
    )
fi

creator_major=4.15
creator_minor=0
creator_file=qt-creator-opensource-linux-x86_64-${creator_major}.${creator_minor}.run
if [ ! -f ./media/${creator_file} ]; then
    (
        echo "Installing QT Dependencies.."
        cd ./media/
        wget http://download.qt.io/official_releases/qtcreator/${creator_major}/${creator_major}.${creator_minor}/${creator_file}  || exit 1
        echo "Success"
        chmod a+x ${creator_file}
    )
fi

tar -xvf ~/media/*.tar -C ~/appimage-workspace/
