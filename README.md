# Krita Developer AppImage Docker Environment

This *Dockerfile* image is based on the official KDE build environment[0]
that in used on KDE CI for building official AppImage packages. This is
a fork of dkazakov's similar build environment[1] but tries to simplify
user interaction by leveraging DockerHub and simplifying the process
of building an appimage.

[0] - https://binary-factory.kde.org/job/Krita_Nightly_Appimage_Dependency_Build/

[1] - https://invent.kde.org/dkazakov/krita-docker-env

## Prerequisites

1) You should have Docker installed. 
**Consult the Docker documentation to see how you should do that for your distro.**

2) You user should be in the `docker` group. If not, run:

```bash
sudo usermod -aG docker username
```

3) You should already have krita's source code cloned somewhere on your system. 
By default this container will reuse your existing krita source tree,
so make sure that it's in a state that reflects the appimage you want to build.
(On master, on a branch, uncommited changes, etc.) The full path to your source
directory will be needed later.

> In this docker image, Krita's dependencies are downloaded on initial
> container run. Importantly, dependency downloading will only happen
> once per container unless the user deletes the archived dependencies. 
> This is in contrast to how the original docker image worked, where
> dependencies were downloaded once by the user and stored in a persistent
> folder for use later within the image. Both of these methods should keep
> bandwith usage to a minimum, but it's important to note.

## Running the latest docker container via DockerHub... Building your first AppImage!

### Initial docker container creation

**For your initial run**, you will need to run something like the following command:

```bash
docker pull eoinoneill1991/krita.appimage;
docker run -it \
  --name krita-appimage \
  --security-opt label:disable \
  -v {DESIRED_OUTPUT_DIR}:/mnt/out \
  -v {KRITA_SRC_DIR}:/mnt/krita \
  eoinoneill1991/krita.appimage
```

{DESIRED_OUTPUT_DIR} is the absolute path on your system where your appimage will be saved. 
{KRITA_SRC_DIR} is the absolute path on your system where Krita's source code resides. These
folders are mounted into the newly created docker container.

For example, on my system, I run:

```bash
docker pull eoinoneill1991/krita.appimage;
docker run -it \
   --name krita-appimage \
   -v /home/eoin/Source/krita/src:/mnt/krita \
   -v /tmp:/mnt/out \
   eoinoneill1991/krita.appimage
```

This example configures the container to save appimages out to my tmp directory, which 
will be automatically cleaned on system reboot. Where you choose to put these appimages
is up to you! :)

Once you've entered the container, it will begin by downloading
dependencies. 

Wait for this process to complete and you should encounter the
familiar bash entry prompt. From here, run the following:

```bash
run_cmake.sh /mnt/krita && build_krita_appimage.sh
```

This will be enough to get your first appimage built and put into the directory of your
choice! Once you've finished, exit the container with the following command:

```bash
exit
```

### Docker container reuse

While the above will give you a single AppImage, here's how you can build an AppImage
without creating a new container.

First, get a list of the containers.

```bash
docker container ls -a
```

You should see one with the `Exited` status with the  name `krita-appimage`. This is 
the container we just created in the last section. Think of this as though you have a
virtual computer that is suspended that you want to wake up.

So lets wake it up!

```bash
docker container start -i krita-appimage
```

This should start the Docker container. You will notice a lot of output while the container extracts
Krita's dependencies again. *If for some reason the dependencies archive is no longer in the container,
it will automatically download them again.*

Finally, making sure your source tree is in the desired state, you can simply 
run the following again to build another appimage.

```bash
run_cmake.sh /mnt/krita && build_krita_appimage.sh
```

## Cleaning up

If you want to clean your Docker of krita images or containers, run:

```bash
docker container rm krita-appimage && docker image rm eoinoneill1991/krita.appimage
```

This should remove the `krita-appimage` container as well as the associated Docker image.


